## ORID

### O:

1.First of all, we conducted a code review of Redux homework. I learned good solutions from my team members.

2.Learned the fundamentals of React Router and practiced route management using React Router on the basis of todoList.

3.I learned to use mockapi to create mock data, use mock data to make api calls, and get the return result or new data.

4.Learn the basics of promises, use async and await to make interface calls, and adjust the execution order

5.Learned to extract custom hooks and reuse them.

### R:

I feel that today is very fulfilling.

### I:

The request interceptor is where error messages are uniformly handled, through this we can learn wrong information.

### D:

I will continue to learn react.

