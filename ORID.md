## ORID

## O:

1.First of all, we conducted a code review of mapper homework. 

2.The teacher added the knowledge about cloud native and had a preliminary understanding of the difference between cloud native and cloud computing.

3.Together with the team members, I reviewed the spring boot knowledge I had learned before by drawing concept map.

4.Learned basic use of flyway. It can be used quickly and efficiently to iterate over database table structures and ensure that data tables remain consistent when deployed to test or production environments.

5.After learning the basics of html and css, using the previously unfamiliar selector through the game of selector, and the game of flexbox, I have updated my understanding of flexbox and gained more understanding of its properties.

6.I learned react hooks and am not familiar with them.

##  R:

I feel that today is very fulfilling, and I am not familiar with react hooks.



## I:

jsx has not been written before, and there is no knowledge of the passing of values before react components.



## D:

I will continue to learn react.