function PromiseNumber(num){
    let promise = new Promise(function(resolve,reject){
        num = 10;
        if(num<10){
            resolve("num小于10，值为："+num);
        }else{
            reject("num大于或等于10，值为："+num);
        }
    });
    return promise;
}
 
PromiseNumber().then(
    function(message){
        console.log(message);
    }
)
.catch(
    function(message){
    console.log(message);
})