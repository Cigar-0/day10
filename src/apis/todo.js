/*
 * @Author: xu xuejia
 * @Date: 2023-07-26 19:00:19
 * @LastEditors: xu xuejia
 * @LastEditTime: 2023-07-27 20:30:55
 * @Description: file content
 * @FilePath: \day10\src\apis\todo.js
 */
import request from "./request";

export const loadTodos = () => {
    return request.get('/todos');
}
export const updateTodo = (id, done) => {
    return request.put(`/todos/${id}`,  {done});
}
export const updateText = (id, text) => {
    return request.put(`/todos/${id}`, {text});
}
export const createTodo = (todo) => {
    return request.post('/todos',todo);
}
export const deleteTodo = (id) => {
    return request.delete(`/todos/${id}`,id);
}
export const findById = (id) => {
    return request.get(`/todos/${id}`)
}