import TodoItem from "./TodoItem";
const TodoGroup = (props) => {
    return (
        <>
            {
                props.todoList.map((value, index) => (<TodoItem key={value.id} value={value} index={index} isEdit={props.isEdit}></TodoItem>))
            }
        </>
    );
};
export default TodoGroup;
