/*
 * @Author: xu xuejia
 * @Date: 2023-07-26 19:00:19
 * @LastEditors: xu xuejia
 * @LastEditTime: 2023-07-26 20:28:55
 * @Description: file content
 * @FilePath: \day10\src\components\Layout.js
 */
import { Outlet } from "react-router-dom";
import { NavLink } from "react-router-dom";
const Layout = () => {
    return ( <div>
        <div className="header">
        <NavLink to="/"> Home</NavLink>
        <NavLink to="/about">About</NavLink>
        <NavLink to="/done">Done</NavLink>
      </div>
        <Outlet/>
        </div> 
    );
}
 
export default Layout;