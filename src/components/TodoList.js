/*
 * @Author: xu xuejia
 * @Date: 2023-07-26 19:00:19
 * @LastEditors: xu xuejia
 * @LastEditTime: 2023-07-26 20:52:45
 * @Description: file content
 * @FilePath: \day10\src\components\TodoList.js
 */
import TodoGroup from "./TodoGroup";
import TodoItemGenerator from "./TodoItemGenerator"
import { useSelector } from "react-redux";
import { useEffect } from "react";
import { useTodo } from "../hooks/useTodo";
const TodoList = () => {

    const todoListNew = useSelector(state => state.todo.todoList);
    const { reloadTodos } = useTodo();

    useEffect(()=> {
      reloadTodos();
    }, [reloadTodos])

    return (
        <div>
            <p style={{fontSize: '30px',fontWeight: 'bold'}}>Todo List</p>
            <TodoGroup todoList={todoListNew} isEdit={true}></TodoGroup>
            <TodoItemGenerator></TodoItemGenerator>
        </div>
    )
}

export default TodoList;
