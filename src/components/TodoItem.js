/*
 * @Author: xu xuejia
 * @Date: 2023-07-25 19:25:02
 * @LastEditors: xu xuejia
 * @LastEditTime: 2023-07-26 20:08:42
 * @Description: file content
 * @FilePath: \day10\src\components\TodoItem.js
 */
import "../style/Todo.css";
import { useNavigate } from "react-router-dom";
import { updateTodo, deleteTodo, updateText } from "../apis/todo";
import { useTodo } from "../hooks/useTodo";
import { CloseCircleOutlined, EditOutlined } from '@ant-design/icons';
import React, { useState } from 'react';
import { Button, Modal, Input } from 'antd';
const TodoItem = (props) => {
  const navigate = useNavigate();
  const { reloadTodos } = useTodo();
  const update = async () => {
    if (props.isEdit) {
      await updateTodo(props.value.id, !props.value.done);
      reloadTodos();
    } else {
      navigate(`/todo/${props.value.id}`);
    }
  };
  const deleteItem = async () => {
    await deleteTodo(props.value.id);
    reloadTodos();
  }
  const [open, setOpen] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState('');
  const showModal = () => {
    setModalText(props.value.text);
    setOpen(true);
  };
  const handleOk = async () => {
    setConfirmLoading(true);
    await updateText(props.value.id, modalText);
    reloadTodos();
    setOpen(false);
    setConfirmLoading(false);
  };
  const handleCancel = () => {
    setOpen(false);
  };
  const handleChangeInput = (e) => {
    setModalText(e.target.value);
  }
  return (
    <div className={props.value.done ? "done-item" : "todo-item"}>
      <span onClick={update}>{props.value.text}</span>
      { props.isEdit &&
      <>

      <Button type="primary" danger shape="circle" size="small" className="edit-button" onClick={deleteItem}>
        <CloseCircleOutlined />
      </Button>
      <Button type="primary" shape="circle" size="small" className="edit-button" onClick={showModal}>
        <EditOutlined />
      </Button>
      <Modal
        title="Update Todo"
        open={open}
        onOk={handleOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
      >
        <Input size="large" style={{ width: '300px' }} value={modalText} placeholder="input todo" onChange={handleChangeInput} />

      </Modal>
      </>}
    </div>
  );
};

export default TodoItem;
