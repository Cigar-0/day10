/*
 * @Author: xu xuejia
 * @Date: 2023-07-25 19:25:03
 * @LastEditors: xu xuejia
 * @LastEditTime: 2023-07-25 21:07:57
 * @Description: file content
 * @FilePath: \day10\src\components\todoSlice.js
 */
import { createSlice } from "@reduxjs/toolkit";
const todoSlice = createSlice({
    name: "todo",
    initialState: {
        todoList: []
    },
    reducers: {
        initTodos: (state, action) => {
            state.todoList = action.payload
        },
        addContent: (state, action) => {
            state.todoList = [...state.todoList, action.payload];
        },
        deleteItem: (state, action) => {
            state.todoList.splice(action.payload.index, 1)
        },
        updateDone: (state, action) => {
        const newItem = state.todoList[action.payload.index]
        newItem.done = !newItem.done 

        state.todoList.slice(0, action.payload.index).concat(newItem).concat(state.todoList.slice(action.payload.index+1))
    }
    }
});

export const { addContent, deleteItem, updateDone, initTodos } = todoSlice.actions;
export default todoSlice.reducer;