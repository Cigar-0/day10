/*
 * @Author: xu xuejia
 * @Date: 2023-07-24 19:55:13
 * @LastEditors: xu xuejia
 * @LastEditTime: 2023-07-27 20:05:36
 * @Description: file content
 * @FilePath: \day10\src\components\TodoItemGenerator.js
 */
import { useState } from "react";
import { createTodo } from "../apis/todo";
import { useTodo } from "../hooks/useTodo";
import { Button, Input } from "antd";
import { CarryOutOutlined } from '@ant-design/icons';

const TodoItemGenerator = () => {
const { reloadTodos } = useTodo();
    const [content, setContent] = useState('');

    const handleChangeInput = (e) => {
        setContent(e.target.value);
    }

    const handleChange = async() => {
        if(content==='') return;
        await createTodo({
            text: content,
            done: false
        })

       reloadTodos();
        setContent('');
    }
    const handleKeyUp = (event) => {
        if(event.keyCode === 13){
            handleChange()
        }
    }

    return (
        <div>
            <Input size="large" style={{width:'300px'}} value={content} placeholder="Please input todo" onChange={handleChangeInput} onKeyUp={handleKeyUp} prefix={<CarryOutOutlined />} />
            <Button type="primary" size="large" onClick={handleChange} className="add-button">add</Button>
        </div>
    )
}

export default TodoItemGenerator;
