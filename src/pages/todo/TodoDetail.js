/*
 * @Author: xu xuejia
 * @Date: 2023-07-26 19:00:19
 * @LastEditors: xu xuejia
 * @LastEditTime: 2023-07-27 20:39:01
 * @Description: file content
 * @FilePath: \day10\src\pages\todo\TodoDetail.js
 */
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Card } from 'antd';
import { findById } from "../../apis/todo";
const TodoDetail = () => {
    const params = useParams()
    const {id} = params;
    // const todo = useSelector((state) => state.todo.todoList.find((todo)=> todo.id === id))
    const [todo, setTodo] = useState();

    useEffect(() => {
        (async()=>{
            const {data} = await findById(id)
            setTodo(data)
        })()
    }, [])
  
    return ( 
    <div>
        <h1>Detail</h1>
        {todo && 
        (
        <div>
    <Card
      style={{ marginTop: 16, margin: 'auto', width:200 }}
      type="inner"
      title="Detail"
    >
      {todo.text}
    </Card>
        </div>
        )
        }
    </div> 
    );
}
 
export default TodoDetail;