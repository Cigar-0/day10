/*
 * @Author: xu xuejia
 * @Date: 2023-07-26 19:00:19
 * @LastEditors: xu xuejia
 * @LastEditTime: 2023-07-26 20:29:59
 * @Description: file content
 * @FilePath: \day10\src\pages\todo\DoneList.js
 */
import TodoGroup from "../../components/TodoGroup";
import { useSelector } from "react-redux";
const DoneList = () => {
    const todoListNew = useSelector(state => state.todo.todoList);
    const doneTodos = todoListNew.filter((todo) => {
        return todo.done;
    })
   
    return ( <div>
        <h1>DoneList</h1>
        <TodoGroup todoList={doneTodos} isEdit={false}></TodoGroup>
    </div> );
}
 
export default DoneList;