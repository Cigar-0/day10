/*
 * @Author: xu xuejia
 * @Date: 2023-07-26 19:00:19
 * @LastEditors: xu xuejia
 * @LastEditTime: 2023-07-26 20:43:36
 * @Description: file content
 * @FilePath: \day10\src\pages\todo\NotFoundPage.js
 */
import { Button, Result } from 'antd';
import { useNavigate } from 'react-router-dom';
const NotFoundPage = () => {
    const navigate = useNavigate()
    return ( 
        <div>
           <Result
    status="404"
    title="404"
    subTitle="Sorry, the page you visited does not exist."
    extra={<Button type="primary" onClick={()=> {navigate('./')}}>Back Home</Button>}
  />
        </div>
     );
}
 
export default NotFoundPage;