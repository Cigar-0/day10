/*
 * @Author: xu xuejia
 * @Date: 2023-07-23 21:14:48
 * @LastEditors: xu xuejia
 * @LastEditTime: 2023-07-24 19:58:47
 * @Description: file content
 * @FilePath: \day10\src\App.js
 */
import './App.css';
import {RouterProvider} from "react-router-dom";
import { router } from './router';
function App() {
  return (
    <div className="App">
      <RouterProvider router={router}></RouterProvider>
    </div>
  );
}

export default App;
