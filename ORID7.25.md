## ORID

### O:

1.First of all, we conducted a code review of React hooks homework. I learned good solutions from my team members.

2.Through the teacher's reexplanation of React-Countner, I have a new understanding of the flow of data between react components.

3.I learned the basic knowledge of redux and made a preliminary use of it.

4.I practiced storing data in redux and modifying it using dispatch.

5.Learned how to write front-end tests, including component tests and ui tests.

### R:

I feel that today is very fulfilling, and I am not familiar with redux.

### I:

Not very familiar with the use of redux.

### D:

I will continue to learn react.